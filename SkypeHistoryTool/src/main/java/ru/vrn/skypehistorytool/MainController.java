/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.vrn.skypehistorytool;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuBar;

/**
 *
 * @author grinev
 */
public class MainController implements Initializable {
    
    @FXML
    private MenuBar menuBar;
    
    @FXML
    private ComboBox<String> statisticsTypeCombo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        statisticsTypeCombo.setPromptText("Statistics type");
        statisticsTypeCombo.setItems(FXCollections.<String>observableArrayList(Arrays.asList("Common statistics", "Chat statistics")));
        statisticsTypeCombo.setValue("Common statistics");
    }
    
}
